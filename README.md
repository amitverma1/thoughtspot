# Thoughtspot AutoComplete Plugin

## CLI Commands

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# test the production build locally
npm run build -- --no-prerender --template src/template.html --production false 

```

Go to the build folder and fire up a python server using 

```
python -m SimpleHTTPServer

```

Launch the assignment on http://0.0.0.0:8000

## PLUG & PLAY
The autocomplete feature has been built as a plugin. You should be able to pick the class and include it in any preact project by just plugging in the api and callback methods. 

For detailed explanation on how things work, checkout the [CLI Readme](https://github.com/developit/preact-cli/blob/master/README.md).