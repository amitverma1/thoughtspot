import { h, Component } from 'preact';
import { Link } from 'preact-router/match';

import onClickOutside from "react-onclickoutside";
import style from './style';

class InputAutoComplete extends Component {
	constructor(props) {
		super(props)

		this.state = {
			empty: true,
			filters: [],
			rowSel: null,
			showAutoComplete: false,
			suggestions: [],
		}
		
		this.handleChange = this.handleChange.bind(this)
		this.handleClickOutside = this.handleClickOutside.bind(this)
		this.handleFocus = this.handleFocus.bind(this)
	}

	render() {
		return (
			<div class={'inputWrapper'}>
				<div class={'inputBox'}>
					{ this.state.filters.length > 0 ?
						<ul class={'filtersList'}>
							{this.state.filters.map(item => {
								return <li dangerouslySetInnerHTML={{__html: item}} />
							})}
						</ul> : null
					}
					<input 
						type={'text'} 
						class={'inputEl'}
						onKeyUp={this.handleChange}
						onFocus={this.handleFocus} />
				</div>
				<ul class={`suggestionsList ${this.state.showAutoComplete ? 'active': ''}`}>
					{ this.state.suggestions.map((item, index) => {
						return <li dangerouslySetInnerHTML={{__html: item}} 
											 class={`${index === this.state.rowSel ? 'active' : ''}`}
											 onClick={this.handleSelection.bind(this, item)} />
					})}
				</ul>
			</div>
		);
	}

	handleClickOutside(event) {
		this.setState({
			showAutoComplete: false
		})
	}
	
	handleChange(e) {
		switch(e.which) {
			case 38: this.handleRowSelection(e, true)
			break;

			case 40: this.handleRowSelection(e, false)
			break;

			case 13: this.state.rowSel !== null && this.handleSelection(this.state.suggestions[this.state.rowSel])
			break;

			case 27: this.setState({ showAutoComplete: false })
			break;

			case 8: 
			this.handleDelete(e)
			this.handleSearch(e)
			break;

			default: this.handleSearch(e)
			break;
		}
	}

	handleSearch(e) {
		let text = e.target && e.target.value
		
		if (text && text !== '') {
			this.props.api(text).then(res => {
				this.setState({
					empty: false,
					rowSel: 0,
					suggestions: this.prep(res, text),
					showAutoComplete: true
				})
			}).catch(e => {
				console.log('api failed', e)
			})
		} else {
			this.setState({
				empty: true,
				rowSel: 0,
				showAutoComplete: false,
				suggestions: [],
			})
		}
	}

	handleDelete(e) {
		if (this.state.empty) {
			let arr = this.state.filters
			arr.pop()
	
			this.setState({
				filters: arr
			})
		}

		if (e.target.value === '') {
			this.setState({
				empty: true,
				rowSel: 0,
				showAutoComplete: false,
				suggestions: [],
			})
		}
	}

	handleFocus(e) {
		if (this.state.suggestions.length && this.state.suggestions.length > 0) {
			this.setState({
				showAutoComplete: true
			})
		}
	}

	handleRowSelection(e, up) {
		e.preventDefault()
		
		let totalSuggestions = this.state.suggestions.length
		
		if (this.state.rowSel !== null) {
			if (up) {
				var selection = this.state.rowSel - 1 < 0 ? 0 : this.state.rowSel - 1
			} else {
				var selection = this.state.rowSel === totalSuggestions - 1 ? this.state.rowSel : this.state.rowSel + 1
			}
		}

		this.setState({
			rowSel: selection || 0,
		})
	}

	handleSelection(item) {
		let arr = this.state.filters

		if (arr.indexOf(item) === -1) {
			arr.push(item)
		}

		this.setState({
			filters: arr
		}, () => {
			if (this.props.cb) {
				this.props.cb(this.state.filters)
			}
		})
	}

	prep(res, search) {
		let arr = []

		res.forEach(item => {
			let match = item.match(search)
			if (match) {
				arr.push(`${item.substr(0, match.index)}<span>${match[0]}</span>${item.substr(match.index + match[0].length)}`)
			} else {
				arr.push(match)
			}
		})

		return arr
	}
}

var clickOutsideConfig = {
  handleClickOutside: function(instance) {
    return instance._component.handleClickOutside;
  }
};

export default onClickOutside(InputAutoComplete, clickOutsideConfig);