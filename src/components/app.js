import { h, Component } from 'preact';
import { Router } from 'preact-router';
import { getSuggestions } from './../utils/api';

import InputAutoComplete from './inputAutoComplete';
import Header from './header';
import Footer from './footer';

export default class App extends Component {
	render() {
		return (
			<div id="app">
				<Header />
				<InputAutoComplete api={getSuggestions} cb={this.handleFilter.bind(this)} />
				<Footer />
			</div>
		);
	}

	handleFilter(filters) {
		console.log('the following filters are applied', filters)
	}
}
