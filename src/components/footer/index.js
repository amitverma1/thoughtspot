import { h, Component } from 'preact';
import { Link } from 'preact-router/match';
import style from './style';

export default class Header extends Component {
	render() {
		return (
			<footer class={style.footer}>
				<div><i class='zmdi zmdi-chevron-up'></i> <span>Navigate Up</span></div>
        <div><i class='zmdi zmdi-chevron-down'></i> <span>Navigate Down</span></div>
        <div><i class='zmdi zmdi-long-arrow-return'></i> <span>Select</span></div>
        <div class={style.esc}><i>ESC</i> <span>Hide List</span></div>
			</footer>
		);
	}
}
